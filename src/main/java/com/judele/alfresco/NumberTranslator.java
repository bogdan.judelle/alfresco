package com.judele.alfresco;

import java.util.function.Function;

public class NumberTranslator {

    private NumberTranslator nextTranslator;
    private final Function<Integer, Boolean> criterion;
    private final Object translation;


    public NumberTranslator(Function<Integer, Boolean> criterion, Object translation) {
        this.criterion = criterion;
        this.translation = translation;
    }

    public Object process(Integer number) {
        if (criterion.apply(number)) {
            return translation;
        }
        return this.hasNextTranslator()
                ? nextTranslator.process(number)
                : number;
    }

    private boolean hasNextTranslator() {
        return nextTranslator != null;
    }

    public void setNextTranslator(NumberTranslator nextTranslator) {
        this.nextTranslator = nextTranslator;
    }

}

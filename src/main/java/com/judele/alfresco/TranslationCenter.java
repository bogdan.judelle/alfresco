package com.judele.alfresco;

public class TranslationCenter {

    private final NumberTranslator firstTranslator;

    public TranslationCenter() {
        Calculator calculator = new Calculator();
        NumberTranslator fizzBuzzTranslator = new NumberTranslator(calculator::isEvenlyDivisibleBy15, "fizzbuzz");
        NumberTranslator fizzTranslator = new NumberTranslator(calculator::isEvenlyDivisibleBy3, "fizz");
        NumberTranslator buzzTranslator = new NumberTranslator(calculator::isEvenlyDivisibleBy5, "buzz");

        fizzBuzzTranslator.setNextTranslator(fizzTranslator);
        fizzTranslator.setNextTranslator(buzzTranslator);

        firstTranslator = fizzBuzzTranslator;
    }

    public Object translate(int number) {
        return firstTranslator.process(number);
    }
}

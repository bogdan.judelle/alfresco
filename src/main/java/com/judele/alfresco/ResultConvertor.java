package com.judele.alfresco;

public class ResultConvertor {

    private static final String SPACE = " ";

    private final Object[] result;

    public ResultConvertor(Object[] result) {
        this.result = result;
    }

    public String asString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object each : result) {
            stringBuilder
                    .append(each)
                    .append(SPACE);
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }
}

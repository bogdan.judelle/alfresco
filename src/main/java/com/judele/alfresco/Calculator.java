package com.judele.alfresco;

public class Calculator {

    public boolean isEvenlyDivisibleBy3(Integer number) {
        return isEvenlyDivisible(number, 3);
    }

    public boolean isEvenlyDivisibleBy5(Integer number) {
        return isEvenlyDivisible(number, 5);
    }

    public boolean isEvenlyDivisibleBy15(Integer number) {
        return isEvenlyDivisible(number, 15);
    }

    private boolean isEvenlyDivisible(Integer dividend, Integer divisor) {
        return dividend % divisor == 0;
    }

}
